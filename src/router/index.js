import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'menuList',
      component: () => import('@/views/menu/list')
    },
    {
      path: '/shell',
      name: 'shellList',
      component: () => import('@/views/shell/list')
    },
    {
      path: '/blog',
      name: 'blogList',
      component: () => import('@/views/blog/list')
    },
    {
      path: '/redis',
      name: 'redisList',
      component: () => import('@/views/redis/list')
    },
    {
      path: '/project',
      name: 'projectList',
      component: () => import('@/views/project/list')
    },
    {
      path: '/grepLog',
      name: 'grepLog',
      component: () => import('@/views/grepLog/list')
    },
    {
      path: '/toolkit',
      name: 'toolkit',
      component: () => import('@/views/toolkit/index')
    },
    {
      path: '/information',
      name: 'information',
      component: () => import('@/views/information/list')
    }
  ]
})

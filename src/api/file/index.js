import axios from '@/libs/api.request'

export const Download = (params) => {
  return axios.request({
    url: '/api/file/Download',
    method: 'get',
    params
  })
}

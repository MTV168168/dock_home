import axios from '@/libs/api.request'

export const getMenuList = (params) => {
  return axios.request({
    url: '/api/menu/GetMenuList',
    method: 'get',
    params
  })
}

export const delMenu = (params) => {
  return axios.request({
    url: '/api/menu/DelMenu',
    method: 'get',
    params
  })
}

export const OperationMenu = (params) => {
  return axios.request({
    url: '/api/menu/OperationMenu',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const ViMenu = (params) => {
  return axios.request({
    url: '/api/menu/ViMenu',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const GetNavigationTypeList = (params) => {
  return axios.request({
    url: '/api/menu/GetNavigationTypeList',
    method: 'get',
    params
  })
}

export const OperationNavigationType = (params) => {
  return axios.request({
    url: '/api/menu/OperationNavigationType',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

export const GetMenuListByType = (params) => {
  return axios.request({
    url: '/api/menu/GetMenuListByType',
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    params
  })
}

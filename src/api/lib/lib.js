import moment from 'moment'
export default {
  dateFormat(data) {
    if (data) {
      // debugger
      // 当前时间转为中国标准时间
      let _nowTime = new Date().toLocaleString('chinese', { hour12: false })
      _nowTime = _nowTime + ' GMT+0800'
      const nowTime = new Date(_nowTime)
      // 将实际时间转为中国标准时间
      const _data_time = new Date(parseInt(data) * 1000).toLocaleString('chinese', { hour12: false })
      const _time = _data_time + ' GMT+0800'
      const realTime = new Date(_time)
      const diffValue = nowTime.getTime() - realTime.getTime()
      if (diffValue < 0) {
        return '刚刚'
      }
      const _min = diffValue / (60 * 1000) // 以min为维度
      if (_min < 1) {
        return '刚刚'
      } else if (_min >= 1 && _min < 10) {
        return '1分钟前'
      } else if (_min >= 10 && _min < 30) {
        return '10分钟前'
      } else if (_min >= 30 && _min < 60) {
        return '半小时前'
      } else if (realTime.getFullYear() === nowTime.getFullYear() && realTime.getMonth() === nowTime.getMonth() && realTime.getDate() === nowTime.getDate()) {
        // 今天
        const minute = realTime.getMinutes() < 10 ? '0' + realTime.getMinutes() : realTime.getMinutes()
        const hour = realTime.getHours() < 10 ? '0' + realTime.getHours() : realTime.getHours()
        return hour + ':' + minute
      } else {
        // 完整时间
        return this.dateFormatFull(data)
      }
    } else {
      // 今天
      return '刚刚'
    }
  },
  dateFormatFull(data) {
    // 完整时间 2021-08-16 10:54:32 - 原生
    // return new Date(parseInt(data) * 1000).toLocaleString('chinese', { hour12: false })
    // 完整时间 2021-08-16 10:54 - 原生
    // return new Date(parseInt(data) * 1000).toLocaleString('chinese', { hour12: false }).replace(/:\d{1,2}$/, ' ')
    // 完整时间 2021-08-16 10:54:32 -- 插件
    return moment.unix(data).format('YYYY-MM-DD HH:mm:ss')
  },
  dateFormatByType(type, data) {
    // 格式化时间戳
    // type:1-完整时间|2-显示最近今天历史
    if (data) {
      switch (parseInt(type)) {
        case 2:
          return this.dateFormat(data)
        default:
        case 1:
          return this.dateFormatFull(data)
      }
    } else {
      return ''
    }
  },
  div(a, b, toFixed) {
    a = parseFloat(a)
    b = parseFloat(b)
    if (a <= 0) {
      return 0
    }
    // toFixed:保留位数
    return (a / b).toFixed(toFixed)
  },
  mul(a, b, toFixed) {
    a = parseFloat(a)
    b = parseFloat(b)
    if (a <= 0 || b <= 0) {
      return 0
    }
    // toFixed:保留位数
    return (a * b).toFixed(toFixed)
  },
  add(a, b, toFixed) {
    a = parseFloat(a)
    b = parseFloat(b)
    if (a <= 0 || b <= 0) {
      return 0
    }
    // toFixed:保留位数
    return (a + b).toFixed(toFixed)
  },
  test() {
    return '122222'
  }
}

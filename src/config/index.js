export default {
  /**
   * @description 配置显示在浏览器标签的title
   */
  title: 'ido-dock',
  /**
   * @description token在Cookie中存储的天数，默认1天
   */
  cookieExpires: 1,
  /**
   * @description 是否使用国际化，默认为false
   *              如果不使用，则需要在路由中给需要在菜单中展示的路由设置meta: {title: 'xxx'}
   *              用来在菜单中显示文字
   */
  useI18n: true,
  /**
   * @description api请求基础路径
   * @description setBaseHostFromHost true:取网站的host|false:取baseUrl
   * @description serverPort 服务器端口后缀
   */
  setBaseHostFromHost: true,
  baseUserSetHost: window.location.hostname, // '127.0.0.1'
  serverPort: ':8089',
  baseUrl: {
    dev: 'http://localhost:8089',
    pro: 'http://localhost:8089'
  },
  /**
   * @description socket基础路径
   * @description setSocketBaseHostFromHost true:取网站的host覆盖|false:不覆盖
   */
  setSocketBaseHostFromHost: true,
  setSocketBaseHost: window.location.hostname, // '127.0.0.1'
  setSocketBasePortUrl: ':8087/socket',
  /**
   * @description 默认打开的首页的路由name值，默认为home
   */
  homeName: 'menu',
  /**
   * @description 需要加载的插件
   */
  plugin: {
    'error-store': {
      showInHeader: true, // 设为false后不会在顶部显示错误日志徽标
      developmentOff: true // 设为true后在开发环境不会收集错误信息，方便开发中排查错误
    }
  }
}

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App'
import router from './router'
import config from '@/config'

Vue.use(ElementUI)
Vue.config.productionTip = false
// 懒加载
import VueLazyLoad from 'vue-lazyload'
Vue.use(VueLazyLoad, {
  preLoad: 1,
  error: './static/img/loading_2.gif',
  loading: './static/img/loading_6.gif',
  attempt: 2
})

/**
 * @description config配置
 */
// 设置当前请求主机名
config.baseUserSetHost = window.location.hostname
if (config.setSocketBaseHostFromHost) {
  // 覆盖socket主机
  config.setSocketBaseHost = window.location.hostname
}

// 复制
import Clipboard from 'v-clipboard'
Vue.use(Clipboard)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
